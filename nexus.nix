{pkgs ? import <nixpkgs> {}, ...}:

pkgs.stdenv.mkDerivation {
  name = "NeXus";
  cmakeFlags = [
    "-DENABLE_CXX=ON"
    "-DENABLE_JAVA=ON"
    "-DENABLE_HDF4=ON"
  ];
  buildInputs = with pkgs; [cmake hdf5 hdf5-cpp hdf5.dev hdf5-cpp.dev libjpeg pkg-config hdf4 hdf4.dev];
  src = pkgs.fetchFromGitHub {
    owner = "nexusformat";
    repo = "code";
    rev = "v4.4.3";
    sha256 = "16h3ag7rm6gvg7dfmgk3qgalnmwc8rvk3dzd5095pybch73rvdq6";
  };
}
