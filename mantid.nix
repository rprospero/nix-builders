{pkgs ? import <nixpkgs> {}}:

let
  # We need an old ipython because 7.17 dropped support for python 3.6

  # ipython = buildPythonPackage rec {
  #   pname = "ipython";
  #   version = "7.16.0";

  #   src = fetchPypi {
  #     inherit pname version;
  #     sha256 = "08fdd5ef7c96480ad11c12d472de21acd32359996f69a5259299b540feba4560";
  #   };
  #   doCheck = false;
  # }
  python = let
    packageOverrides = self: super: {
      ipython = super.ipython.overridePythonAttrs(old: rec {
        version = "7.14.0";
        src =  super.fetchPypi {
          pname = "ipython";
          inherit version;
          sha256 = "077vn12mi1a4mkkd93bak273h1yq1qbrw25k5y2xlngrs20nf4ph";
        };
      });
      notebook = super.notebook.overridePythonAttrs(old: {doCheck = false;});
    };
  in pkgs.python36.override {inherit packageOverrides; self = python;};
  mython = python.withPackages (pkgs: with pkgs; [
      boost
      h5py
      ipython
      jupyter
      numpy
      matplotlib
      periodictable
      psutil
      pyqt5
      pylint
      pyyaml
      qscintilla-qt5
      qtconsole
      qtpy
      scipy
      six
      # sip
      sphinx
      sphinx_bootstrap_theme
      toml
  ]);
  # mython = (pkgs.python36Full.buildEnv.override {
  #   extraLibs = with pkgs.python36Packages; [
  #     boost
  #     h5py
  #     ipython
  #     jupyter
  #     numpy
  #     matplotlib
  #     periodictable
  #     psutil
  #     pyqt5
  #     pylint
  #     pyyaml
  #     qscintilla-qt5
  #     qtconsole
  #     qtpy
  #     scipy
  #     six
  #     sip
  #     sphinx
  #     sphinx_bootstrap_theme
  #     toml
  #   ];
  #   ignoreCollisions = true;
  # });

  # qtconsole = pkgs.python36Packages.buildPythonPackage rec {
  #   pname = "qtconsole";
  #   version = "4.6.0";

  #   src = pkgs.python36Packages.fetchPypi {
  #     inherit pname version;
  #     sha256 = "1ldw8jvs3hghy54sq2cr6cn79kzddb3qxb3gnalydpz7c8v44kv5";
  #   };

  #   doCheck = false;

  #   buildInputs = with pkgs.python36Packages; [pyqt5 jupyter qtpy];
  # };

  sphinx_bootstrap_theme = python.pkgs.buildPythonPackage rec {
    pname = "sphinx-bootstrap-theme";
    version = "0.7.1";

    src = python.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "024x4w38h4h0aac58nssqkd7mg3fdf14m8kaaw36qk3dnz6467jp";
    };

    doCheck = false;

    buildInputs = with python.pkgs; [sphinx pytest];
  };

  NeXus = import ./nexus.nix {};


in
  pkgs.mkShell {
    nativeBuildInputs = [pkgs.doxygen pkgs.qt5.qtbase pkgs.qt5.qtmultimedia pkgs.qt5.qttools pkgs.qt5.qtwebengine];
    buildInputs = [
      pkgs.boost
      pkgs.boost.dev
      pkgs.ccache
      pkgs.clang-tools
      pkgs.cmake
      pkgs.eigen
      pkgs.feh
      pkgs.git
      pkgs.gsl
      pkgs.gtest

      pkgs.hdf5
      pkgs.hdf5-cpp
      pkgs.hdf5.dev
      pkgs.hdf5-cpp.dev

      pkgs.jemalloc
      pkgs.jsoncpp
      pkgs.libGL
      pkgs.libGLU
      pkgs.libgcc
      pkgs.libsForQt5.qscintilla
      pkgs.muparser
      NeXus
      pkgs.opencascade
      pkgs.openssl
      pkgs.pkg-config
      pkgs.poco
      mython
      python.pkgs.boost.dev
      python.pkgs.numpy
      pkgs.qtcreator
      pkgs.rdkafka
      pkgs.tbb
      pkgs.zlib
    ];
    BOOST_ROOT = "${python.pkgs.boost}/lib";
    POCO_ROOT = "${pkgs.poco}/";
    GMock_DIR = "${pkgs.gmock}/";
    Python3_ROOT_DIR = "${python}/";
    PYQT5_SIP_DIR = "${python.pkgs.pyqt5}/share/sip/PyQt5";
    SPHINX_PACKAGE_DIR = "${python.pkgs.sphinx}/";
    QSCINTILLA_QT5_LIBRARY = "${python.pkgs.qscintilla-qt5}";
    QSCINTILLA_QT5_INCLUDE_DIR = "${pkgs.libsForQt5.qscintilla}/include";
  }
