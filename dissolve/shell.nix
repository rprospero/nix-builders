{pkgs ? import <nixpkgs> {}}:

let
  qt6 = import ./qt6.nix {inherit pkgs;};
  weggli = import ./weggli.nix {inherit pkgs;};
in

pkgs.stdenv.mkDerivation rec {
  name = "dissolve-shell";
    buildInputs = [
    pkgs.antlr4
    pkgs.antlr4.runtime.cpp
    pkgs.bison
    pkgs.ccache
    pkgs.ccls
    (pkgs.clang-tools.override {llvmPackages = pkgs.llvmPackages;})
    pkgs.cli11
    pkgs.cmake
    pkgs.cmake-format
    pkgs.cmake-language-server
    pkgs.conan
    pkgs.distcc
    pkgs.freetype
    pkgs.ftgl
    pkgs.gcc10
    pkgs.gdb
    pkgs.libGL
    pkgs.libglvnd
    pkgs.libglvnd.dev
    pkgs.libuuid
    pkgs.ninja
    pkgs.openmpi
    pkgs.qt5.full
    pkgs.pkg-config
    pkgs.shake
    pkgs.valgrind
    weggli
    pkgs.xorg.libX11
    pkgs.xorg.libfontenc
    pkgs.xorg.libICE
    pkgs.xorg.libSM
    pkgs.xorg.libXau
    pkgs.xorg.libXaw
    pkgs.xorg.libXcomposite
    pkgs.xorg.libXcursor
    pkgs.xorg.libXdamage
    pkgs.xorg.libXdmcp
    pkgs.xorg.libXext
    pkgs.xorg.libXft
    pkgs.xorg.libXi
    pkgs.xorg.libXinerama
    pkgs.xorg.libxkbfile
    pkgs.xorg.libXpm
    pkgs.xorg.libXrandr
    pkgs.xorg.libXres
    pkgs.xorg.libXScrnSaver
    pkgs.xorg.libXtst
    pkgs.xorg.libXv
    pkgs.xorg.libXvMC
    pkgs.xorg.libXxf86vm
    pkgs.xorg.xtrans
    pkgs.xorg.xtrans
    pkgs.xorg.libxcb
    pkgs.xorg.xcbutil
    pkgs.xorg.xcbutilwm
    pkgs.xorg.xcbutilimage
    pkgs.xorg.xcbutilkeysyms
    pkgs.xorg.xcbutilrenderutil
    pkgs.xkeyboard_config
  ];
    CMAKE_CXX_COMPILER_LAUNCHER="${pkgs.ccache}/bin/ccache";
    CXXL = "${pkgs.stdenv.cc.cc.lib}";
    QTDIR="${qt6}/6.1.1/gcc_64";
    Qt6_DIR="${QTDIR}/lib/cmake/Qt6";
    Qt6CoreTools_DIR="${QTDIR}/lib/cmake/Qt6CoreTools";
    Qt6GuiTools_DIR="${QTDIR}/lib/cmake/Qt6GuiTools";
    Qt6WidgetsTools_DIR="${QTDIR}/lib/cmake/Qt6WidgetsTools";
    PATH="${QTDIR}/bin";
}
