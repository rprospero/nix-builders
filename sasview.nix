{pkgs ? import <nixpkgs> {}}:

let
  python = (pkgs.python37Full.buildEnv.override {
    extraLibs = with pkgs.python37Packages; [
      boost
      bumps
      h5py
      ipython
      jupyter
      lxml
      numpy
      matplotlib
      periodictable
      psutil
      pyqt5
      pylint
      pyyaml
      qscintilla-qt5
      qt5reactor
      qtconsole
      qtpy
      scipy
      sip
      sphinx
      sphinx_bootstrap_theme
      twisted
      xhtml2pdf
    ];
    ignoreCollisions = true;
  });

  qtconsole = pkgs.python37.pkgs.buildPythonPackage rec {
    pname = "qtconsole";
    version = "4.6.0";

    src = pkgs.python37.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "1ldw8jvs3hghy54sq2cr6cn79kzddb3qxb3gnalydpz7c8v44kv5";
    };

    doCheck = false;

    buildInputs = with pkgs.python37Packages; [pyqt5 jupyter qtpy];
  };

  sphinx_bootstrap_theme = pkgs.python37.pkgs.buildPythonPackage rec {
    pname = "sphinx-bootstrap-theme";
    version = "0.7.1";

    src = pkgs.python37.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "024x4w38h4h0aac58nssqkd7mg3fdf14m8kaaw36qk3dnz6467jp";
    };

    doCheck = false;

    buildInputs = with pkgs.python37Packages; [sphinx pytest];
  };

  qt5reactor = pkgs.python37.pkgs.buildPythonPackage rec {
    pname = "qt5reactor";
    version = "0.6.1";

    src = pkgs.python37.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "1w1ng0ynz4xsad6vnm1jlxgm3kcxf77xqfdsrycfl7l9z9gy80lz";
    };

    doCheck = false;

    buildInputs = with pkgs.python37Packages; [qtpy twisted];
  };

  NeXus = import ./nexus.nix {};


in
  pkgs.mkShell {
    nativeBuildInputs = [pkgs.doxygen python pkgs.qt5.full];
    buildInputs = [
      python
    ];
    PYQT5_SIP_DIR = "${pkgs.python37Packages.pyqt5}/share/sip/PyQt5";
    SPHINX_PACKAGE_DIR = "${pkgs.python37Packages.sphinx}/";
  }
