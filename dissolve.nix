let
  pkgs = import <nixpkgs> {};
in
pkgs.stdenvNoCC.mkDerivation {
  name = "dissolve-shell";
  nativeBuildInputs = with pkgs.qt5; [wrapQtAppsHook];
  buildInputs = [
    pkgs.antlr4
    pkgs.antlr4.runtime.cpp
    pkgs.bison
    pkgs.ccache
    pkgs.ccls
    (pkgs.clang-tools.override {llvmPackages = pkgs.llvmPackages;})
    pkgs.cli11
    pkgs.cmake
    pkgs.cmake-format
    pkgs.cmake-language-server
    pkgs.conan
    pkgs.distcc
    pkgs.freetype
    pkgs.ftgl
    pkgs.gcc9
    pkgs.gdb
    pkgs.libGL
    pkgs.libglvnd
    pkgs.libglvnd.dev
    pkgs.ninja
    pkgs.openmpi
    pkgs.pugixml
    pkgs.qt514.full
    pkgs.tbb
    pkgs.valgrind
  ];
  CCACHE_PREFIX="distcc";
  TBB_ROOT = "${pkgs.tbb}";
  TBB_LIB = "${pkgs.tbb}/lib";
}
