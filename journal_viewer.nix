{ stdenv ? (import <nixpkgs> { }).stdenv, lib ? (import <nixpkgs> { }).lib
, pkgs ? (import <nixpkgs> { }).pkgs}:

pkgs.qt5.mkDerivation rec {
  version_major = "1";
  version_minor = "8";
  version_patch = "4";
  version = "${version_major}.${version_minor}.${version_patch}";
  version_int = "${version_major}${version_minor}${version_patch}";
  name = "journal-viewer-${version}";
  src = fetchTarball "https://www.projectaten.com/user/pages/jv/download/_version${version_int}/Source/jv-${version}.tar.gz";
  configureFlags = [
    "--with-qtrcc=${pkgs.qt5.full}/bin/rcc"
    "--with-qtuic=${pkgs.qt5.full}/bin/uic"
    "--with-qtmoc=${pkgs.qt5.full}/bin/moc"
  ];
  installPhase = ''
	make desktopdir=$out/share/applications icondir=$out/share/pixmaps install
  '';
  nativeBuildInputs = [
    pkgs.buildPackages.gfortran
    pkgs.buildPackages.stdenv.cc
    pkgs.qt5.qtbase
  ];
  buildInputs = with pkgs; [
    hdf5
    libtool
    pkg-config
  ];
}
